from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.chrome.options import Options
import time
import re
from bs4 import BeautifulSoup
from html import escape
from datetime import datetime
from pathlib import Path
from git import Repo


DESCRICAO = True
#DESCRICAO = False ###
MINIMO_DE_PONTOS_PARA_DESCRICAO = 5
PUBLICAR = True

# Set up the Selenium options
chrome_options = Options()
# Run Chrome in headless mode
#chrome_options.add_argument("--headless")  

# set up the Chrome driver
# https://chromedriver.chromium.org/downloads
driver = webdriver.Chrome(options=chrome_options)

# create a new HTML file and add a table element
html_template = '''
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7M7J26ZX9G"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-7M7J26ZX9G');
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Milhas</title>
  <link href="css/theme.blue.css" rel="stylesheet"/>
  <script src="js/jquery-3.7.0.min.js">
  </script>
  <script src="js/jquery.tablesorter.js">
  </script>
<style>
	/* override the vertical-align top in the blue theme */
	.notes.tablesorter-blue tbody td { vertical-align: middle; }
	</style>

<script src="js/jquery.tablesorter.widgets.js"></script>


<script id="js">
$(function() {

	// call the tablesorter plugin
	$("#table").tablesorter({
    
        // initial sort columns (3rd desc and 2nd asc)
        sortList : [[2,1], [1,0]] ,

		theme: 'blue',

		// hidden filter input/selects will resize the columns, so try to minimize the change
		widthFixed : false,

		// initialize zebra striping and filter widgets
		widgets: ["zebra", "filter", "resizable"],

		ignoreCase: false,

		widgetOptions : {

            // headers widths applied at initialization & resizable reset
            // this setting includes any non-resizable cells (resizable-false)
            //resizable_widths : [ '10%', '10%', '50px' ]
        
			// filter_anyMatch options was removed in v2.15; it has been replaced by the filter_external option

			// If there are child rows in the table (rows with class name from "cssChildRow" option)
			// and this option is true and a match is found anywhere in the child row, then it will make that row
			// visible; default is false
			filter_childRows : false,

			// if true, filter child row content by column; filter_childRows must also be true
			filter_childByColumn : false,

			// if true, include matching child row siblings
			filter_childWithSibs : true,

			// if true, a filter will be added to the top of each table column;
			// disabled by using -> headers: { 1: { filter: false } } OR add class="filter-false"
			// if you set this to false, make sure you perform a search using the second method below
			filter_columnFilters : true,

			// if true, allows using "#:{query}" in AnyMatch searches (column:query; added v2.20.0)
			filter_columnAnyMatch: true,

			// extra css class name (string or array) added to the filter element (input or select)
			filter_cellFilter : '',

			// extra css class name(s) applied to the table row containing the filters & the inputs within that row
			// this option can either be a string (class applied to all filters) or an array (class applied to indexed filter)
			filter_cssFilter : '', // or []

			// add a default column filter type "~{query}" to make fuzzy searches default;
			// "{q1} AND {q2}" to make all searches use a logical AND.
			filter_defaultFilter : {},

			// filters to exclude, per column
			filter_excludeFilter : {},

			// jQuery selector (or object) pointing to an input to be used to match the contents of any column
			// please refer to the filter-any-match demo for limitations - new in v2.15
			filter_external : '',

			// class added to filtered rows (rows that are not showing); needed by pager plugin
			filter_filteredRow : 'filtered',

			// ARIA-label added to filter input/select; {{label}} is replaced by the column header
			// "data-label" attribute, if it exists, or it uses the column header text
			filter_filterLabel : 'Filter "{{label}}" column by...',

			// add custom filter elements to the filter row
			// see the filter formatter demos for more specifics
			filter_formatter : null,

			// add custom filter functions using this option
			// see the filter widget custom demo for more specifics on how to use this option
			filter_functions : null,

			// hide filter row when table is empty
			filter_hideEmpty : false,

			// if true, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
			// below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
			// in v2.26.6, this option will also accept a function
			filter_hideFilters : false,

			// Set this option to false to make the searches case sensitive
			filter_ignoreCase : true,

			// if true, search column content while the user types (with a delay).
			// In v2.27.3, this option can contain an
			// object with column indexes or classnames; "fallback" is used
			// for undefined columns
			filter_liveSearch : true,

			// global query settings ('exact' or 'match'); overridden by "filter-match" or "filter-exact" class
			filter_matchType : { 'input': 'exact', 'select': 'exact' },

			// a header with a select dropdown & this class name will only show available (visible) options within that drop down.
			filter_onlyAvail : 'filter-onlyAvail',

			// default placeholder text (overridden by any header "data-placeholder" setting)
			filter_placeholder : { search : '', select : '' },

			// jQuery selector string of an element used to reset the filters
			filter_reset : 'button.reset',

			// Reset filter input when the user presses escape - normalized across browsers
			filter_resetOnEsc : true,

			// Use the $.tablesorter.storage utility to save the most recent filters (default setting is false)
			filter_saveFilters : false,

			// Delay in milliseconds before the filter widget starts searching; This option prevents searching for
			// every character while typing and should make searching large tables faster.
			filter_searchDelay : 300,

			// allow searching through already filtered rows in special circumstances; will speed up searching in large tables if true
			filter_searchFiltered: true,

			// include a function to return an array of values to be added to the column filter select
			filter_selectSource  : null,

			// if true, server-side filtering should be performed because client-side filtering will be disabled, but
			// the ui and events will still be used.
			filter_serversideFiltering : false,

			// Set this option to true to use the filter to find text from the start of the column
			// So typing in "a" will find "albert" but not "frank", both have a's; default is false
			filter_startsWith : false,

			// Filter using parsed content for ALL columns
			// be careful on using this on date columns as the date is parsed and stored as time in seconds
			filter_useParsedData : false,

			// data attribute in the header cell that contains the default filter value
			filter_defaultAttrib : 'data_value',

			// filter_selectSource array text left of the separator is added to the option value, right into the option text
			filter_selectSourceSeparator : '|'

		}

	});

	// Clear stored filters - added v2.25.6
	$('.resetsaved').click(function() {
		$('#table').trigger('filterResetSaved');

		// show quick popup to indicate something happened
		var $message = $('<span class="results"> Reset</span>').insertAfter(this);
		setTimeout(function() {
			$message.remove();
		}, 500);
		return false;
	});

	// External search
	// buttons set up like this:
	// <button type="button" data-filter-column="4" data-filter-text="2?%">Saved Search</button>
	$('button[data-filter-column]').click(function() {
		/*** first method *** data-filter-column="1" data-filter-text="!son"
			add search value to Discount column (zero based index) input */
		var filters = [],
			$t = $(this),
			col = $t.data('filter-column'), // zero-based index
			txt = $t.data('filter-text') || $t.text(); // text to add to filter

		filters[col] = txt;
		// using "table.hasFilters" here to make sure we aren't targeting a sticky header
		$.tablesorter.setFilters( $('#table'), filters, true ); // new v2.9

		/** old method (prior to tablsorter v2.9 ***
		var filters = $('table.tablesorter').find('input.tablesorter-filter');
		filters.val(''); // clear all filters
		filters.eq(col).val(txt).trigger('search', false);
		******/

		/*** second method ***
			this method bypasses the filter inputs, so the "filter_columnFilters"
			option can be set to false (no column filters showing)
		******/
		/*
		var columns = [];
		columns[5] = '2?%'; // or define the array this way [ '', '', '', '', '', '2?%' ]
		$('table').trigger('search', [ columns ]);
		*/

		return false;
	});

});
</script>

<script>
    $(function() {
    
        // *** widgetfilter_startsWith toggle button ***
        $('button.toggle').click(function() {
            var c = $('#table')[0].config,
            $t = $(this),
            // toggle the boolean
            fsw = !c.widgetOptions.filter_startsWith,
            fic = !c.widgetOptions.filter_ignoreCase;
            if ($t.hasClass('fsw')) {
                c.widgetOptions.filter_startsWith = fsw;
                $('#start').html(fsw.toString());
            } else if ($t.hasClass('fic')) {
                c.widgetOptions.filter_ignoreCase = fic;
                $('#case').html(fic.toString());
            } else {
                $t = c.$headers.eq(1).toggleClass('filter-match');
                $t.find('span').html( $t.hasClass('filter-match') ? '' : ' No' );
            }
            // update search after option change; add false to trigger to skip search delay
            c.$table.trigger('search', false);
            return false;
        });
    
    });
    
</script>
</head>
<body>
<p></p>
<table id="table">
  <thead></thead>
  <tbody></tbody>
</table>
</body>
</html>
'''

def stripHtml(data):
    p = re.compile(r'<.*?>')
    p_2 = p.sub('', data)
    p_3 = p_2.replace('&nbsp;',' ')
    return(p_3)

# scrape Livelo site
def scrape_livelo(url):
    # navigate to the Livelo page
    driver.get(url)
    time.sleep(5)

    # sort by biggest reward
    select_element = driver.find_element(By.ID, 'slct-filterSort')
    select = Select(select_element)
    select.select_by_visible_text('Maior Pontuação')
    time.sleep(5)

    # find all div elements with the id "div-parity"
    divs = driver.find_elements(By.ID, "div-parity")

    partners = []
    rewards = []
    links = []
    texts = []

    # loop through each div and extract the partner name and reward
    for div in divs:

        # find the partner
        partner = div.find_element(By.ID, "img-parityImg").get_attribute("alt")
        partners.append(partner)

        # find the reward
        reward = div.find_element(By.ID, "div-parityInfo").text
        reward_numberes_no_dot = reward.replace('.', '')
        reward_numbers_str = re.findall('\d+',reward_numberes_no_dot)
        reward_numbers_int = map(int, reward_numbers_str)
        reward_max = max(reward_numbers_int)
        reward_max_str = str(reward_max)
        rewards.append(reward_max_str)

        # get the link
        link = div.find_element(By.TAG_NAME, "a").get_attribute('href')
        links.append(link)

    # loop through each link and extract the text
    for link, reward in zip(links, rewards):
        if ( (DESCRICAO) and (int(reward) >= MINIMO_DE_PONTOS_PARA_DESCRICAO) ):
            driver.get(link)
            time.sleep(5)
            try:
                text = driver.find_element(By.ID, "div-bannerParityText").text
                texts.append(text)
            except:
                texts.append("")
        else:
            texts.append("")

    return(partners, rewards, links, texts)

# scrape Esfera site
def scrape_esfera(url):
    # navigate to the Livelo page
    driver.get(url)
    # wait for the page to load
    time.sleep(5)

    # sort by biggest reward
    select_element = driver.find_element(By.CLASS_NAME, 'selectPartners')
    select = Select(select_element)
    select.select_by_value('pointsDesc')

    # wait for the page to load
    time.sleep(5)

    # find all div elements with the id "div-parity"
    divs = driver.find_elements(By.CLASS_NAME, 'PartnerShopv2List-partners__marginPartners')

    partners = []
    rewards = []
    links = []
    texts = []

    # loop through each div and extract the partner name and reward
    for div in divs:

        # find the partner
        partner = div.find_element(By.CLASS_NAME, "-partnerName").text
        partners.append(partner)

        # find the reward
        reward = div.find_element(By.CLASS_NAME, "points").text
        reward_numberes_no_dot = reward.replace('.', '')
        reward_numbers_str = re.findall('\d+',reward_numberes_no_dot)
        reward_numbers_int = map(int, reward_numbers_str)
        reward_max = max(reward_numbers_int)
        reward_max_str = str(reward_max)
        rewards.append(reward_max_str)

        # get the link
        link = div.find_element(By.TAG_NAME, "a").get_attribute('href')
        links.append(link)

    # loop through each link and extract the text
    for link, reward in zip(links, rewards):
        if ( (DESCRICAO) and (int(reward) >= MINIMO_DE_PONTOS_PARA_DESCRICAO) ):
            driver.get(link)
            time.sleep(5)
            #text = driver.find_element(By.CLASS_NAME, "-miniaccordion").text
            div_ul = driver.find_element(By.CLASS_NAME, "-miniaccordioncontent")
            text = div_ul.get_attribute('innerHTML')
            #div_ul_attr = driver.find_element(By.CLASS_NAME, "-miniaccordioncontent").get_attribute()
            #print(div_ul_attr)
            texts.append(stripHtml(text))
        else:
            texts.append("")

    return(partners, rewards, links, texts)

soup = BeautifulSoup(html_template, 'html.parser')

# add time to the page
def outputPageTime():
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    time_div = soup.new_tag("div")
    time_div.string = "Atualizado em: "+dt_string
    soup.p.append(time_div)

# add table headers
def outputPageTableHeader():
    header_row = soup.new_tag('tr')

    # header_columns = ['Programa', 'Ordem', 'Parceiro', 'Pontos (máx)', 'Descrição']
    # Programa
    th = soup.new_tag('th', **{'data-placeholder':'L*|E*'})
    th.string = 'Programa'
    header_row.append(th)

    # Ordem
#order#    th = soup.new_tag('th', **{'data-placeholder':'no site'})
#order#    th.string = 'Ordem'
#order#    header_row.append(th)

    # Parceiro
    th = soup.new_tag('th', **{'data-placeholder':'Filtro'})
    th.string = 'Parceiro'
    header_row.append(th)

    # Pontos (máx)
    th = soup.new_tag('th', data_value='>5 AND <50', **{'data-placeholder':'Ex: >5 AND <50'})
    #th = soup.new_tag('th', **{'data-placeholder':'Ex: >10 AND <50'})
    #th = soup.new_tag('th', data_value='>5 AND <50')
    th.string = 'Pontos (máx)'
    header_row.append(th)

    # Descrição
    th = soup.new_tag('th', **{'data-placeholder':'Filtro'})
    th.string = 'Descrição'
    header_row.append(th)
    
    soup.select_one('thead').append(header_row)

def add_table_lines(program_name, partners, rewards, links, texts):
    # loop through each partner and add a row to the table with the order, partner (with link), reward, and text
    for i in range(len(partners)):

        row = soup.new_tag('tr')

        program_col = soup.new_tag('td')
        program_col.string = program_name

#order#        order_col = soup.new_tag('td')
#order#        order_col.string = str(i+1)

        link_col = soup.new_tag('td')
        link = soup.new_tag('a', href=links[i])
        link.string = escape(partners[i])
        link_col.append(link)

        reward_col = soup.new_tag('td')
        reward_col.string = escape(rewards[i])

        text_col = soup.new_tag('td')
        try:
            text_col.string = escape(texts[i])
        except:
            text_col.string = ""

        row.append(program_col)
#order#        row.append(order_col)
        row.append(link_col)
        row.append(reward_col)
        row.append(text_col)

        soup.select_one('tbody').append(row)

# write the HTML to a file
def saveHTML(name):
    with open('www/'+name+'.html', 'w', encoding='utf-8') as f:
        f.write(str(soup.prettify()))

def publish_web_page():
    p = Path('.')
    repo = Repo(p)
    #repo.git.add('www/index.html')
    repo.git.add(all=True)
    repo.git.commit('-m', 'update', author='982fer@gmail.com')
    repo.git.push('origin', 'main')




outputPageTime()
outputPageTableHeader()
livelo_partners,livelo_rewards,livelo_links,livelo_texts = scrape_livelo("https://www.livelo.com.br/ganhe-pontos-compre-e-pontue")
add_table_lines("Livelo",livelo_partners,livelo_rewards,livelo_links,livelo_texts)
esfera_partners,esfera_rewards,esfera_links,esfera_texts = scrape_esfera("https://www.esfera.com.vc/c/junte-pontos/junte-pontos/esf02163")
add_table_lines("Esfera",esfera_partners,esfera_rewards,esfera_links,esfera_texts)
saveHTML("index")

# if publicar true e milhas novas != milhas antigas
publish_web_page()

# close the driver
driver.quit()






####def send_to_telegram(message):
####    # @milhas_pontos_bot
####    apiToken = '5963362140:AAFpXlcBPjWWz99CaXQf04bgwJcsrR-ymDg'
####    # Grupo: Alertas de Milhas e Pontos
####    chatID = '-968607582'
####    apiURL = f'https://api.telegram.org/bot{apiToken}/sendMessage'
####    try:
####        response = requests.post(apiURL, json={'chat_id': chatID, 'text': message})
####        #print(response.text)
####    except Exception as e:
####        print(e)